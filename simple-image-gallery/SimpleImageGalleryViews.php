<?php

class SimpleImageGalleryViews
{

    /**
     * Render settings page
     *
     * @return void
     */
    public static function createSettingsPage()
    {
        echo '
        <div class="wrap">
            <h1>Simple Image Gallery Settings</h1>
            <form method="post" action="options.php">';
        settings_fields('simple-image-gallery-option-group');
        do_settings_sections('simple-image-gallery-settings');
        submit_button();

        echo '</form>
        </div>';
    }

    /**
     * Render Show Title checkbox input
     *
     * @param string $option
     * @param string $key
     * @param bool $value
     * @return void
     */
    public static function showTitleField($option, $key, $value)
    {
        $checkStatus = $value ? 'checked' : '';
        printf('<input type="checkbox" name="%s[%s]" value="1" %s />', $option, $key, $checkStatus);
    }

    /**
     * Render shorcode in frontend
     *
     * @param bool $showTitle
     * @param array $images
     * @return void
     */
    public static function renderShortCode($showTitle, $images)
    {
        $buildHTML = '<div class="simple-image-gallery">';
        foreach ($images as $image) {
            $buildHTML .= '<div data-image="' . $image->url . '" class="sample-image-gallery-item">';
            $buildHTML .= '<img src="' . $image->thumbnailUrl . '" >';
            if ($showTitle) {
                $buildHTML .= '<div class="sample-image-gallery-item-title">' . $image->title . '</div>';
            }
            $buildHTML .= '</div>';
        }
        $buildHTML .= '</div>';
        return $buildHTML;
    }

}
