<?php
/**
 * Plugin Name: Simple Image Gallery
 * Plugin URI: #
 * Description: Coding challenge Newsletter2go
 * Version: 1.0
 * Author: Rukshan Fernando, Sri Lanka
 * Author URI: mailto:ruk.fernando93@gmail.com
 **/

include_once __DIR__ . '/SimpleImageGallery.php';
include_once __DIR__ . '/SimpleImageGalleryViews.php';

/**
 * create ImageGallery Instance
 */
$simpleImageGallery = new SimpleImageGallery();
