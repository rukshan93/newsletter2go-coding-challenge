<?php

class SimpleImageGallery
{

    private $options;

    /**
     * Construct Simple Image Galley Plugin
     */
    public function __construct()
    {
        $this->fetchOptions();
        $this->setSettingsPage();
        $this->setStylesAndScripts();
        $this->setShortCode();
    }
    /**
     * Retrive saved options from WP Database
     *
     * @return void
     */
    public function fetchOptions()
    {
        $defaultValues = array(
            'show_title' => false,
        );
        $this->options = get_option('simple-image-galery');
        if (is_array($this->options)) {
            $this->options = array_merge($defaultValues, $this->options);
        } else {
            $this->options = $defaultValues;
        }
    }

    /**
     * Define shortcode [simple-image-gallery]
     *
     * @return void
     */
    public function setShortCode()
    {
        add_shortcode('simple-image-gallery', array($this, 'renderShortCode'));
    }

    /**
     * Render shortcode
     *
     * @param array $args
     * @return string
     */
    public function renderShortCode($args)
    {
        $images = $this->fetchImages();
        return SimpleImageGalleryViews::renderShortCode($this->options['show_title'], $images);
    }

    /**
     * Define settings page hooks
     *
     * @return void
     */
    public function setSettingsPage()
    {
        add_action('admin_menu', array($this, 'createSettingsPageMenu'));
        add_action('admin_init', array($this, 'createSettingsPage'));
    }

    /**
     * Create settings page menu
     *
     * @return void
     */
    public function createSettingsPageMenu()
    {
        add_options_page(
            'Simple Image Gallery',
            'Simple Image Gallery',
            'manage_options',
            'simple-image-gallery-settings',
            array(SimpleImageGalleryViews::class, 'createSettingsPage')
        );
    }

    /**
     * Create settings page
     *
     * @return void
     */
    public function createSettingsPage()
    {
        register_setting(
            'simple-image-gallery-option-group',
            'simple-image-galery'
        );

        add_settings_section(
            'simple-image-gallery-settings-section',
            'Visibility options',
            '',
            'simple-image-gallery-settings'
        );

        add_settings_field(
            'show_title',
            'Show Title',
            array($this, 'renderTitleField'),
            'simple-image-gallery-settings',
            'simple-image-gallery-settings-section'
        );
    }

    /**
     * Create settings page show_title input field
     *
     * @return void
     */
    public function renderTitleField()
    {
        SimpleImageGalleryViews::showTitleField('simple-image-galery',
            'show_title',
            $this->options['show_title']);
    }

    /**
     * Create hook to add scripts and styles in frontend
     *
     * @return void
     */
    public function setStylesAndScripts()
    {
        add_action('wp_enqueue_scripts', array($this, 'registerStylesAndScripts'));
    }

    /**
     * Add styles and script in frontend
     *
     * @return void
     */
    public function registerStylesAndScripts()
    {
        wp_register_style('simple-image-gallery', plugins_url('simple-image-gallery/css/styles.css'));
        wp_enqueue_style('simple-image-gallery');
    }

    /**
     * Fetch images from API endpoint
     *
     * @return array
     */
    public function fetchImages()
    {
        try {
            $curl = curl_init();

            curl_setopt_array(
                $curl,
                [
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_URL => 'https://jsonplaceholder.typicode.com/photos',
                ]
            );
            $response = curl_exec($curl);
            curl_close($curl);
            $data = json_decode($response);
            $data_keys = array_rand($data, 10);

            $return = array();

            foreach ($data_keys as $key) {
                $return[] = $data[$key];
            }
            return $return;
        } catch (Exception $ex) {
            return array();
        }

    }

}
